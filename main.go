package main

import (
	"flag"
	"log"
	"net/http"
	"net/smtp"
	"strconv"

	"github.com/julienschmidt/httprouter"
)

func main() {

	var emailp = flag.String("email", "", "The email to authenticate against the mail server with, and to send message to.")
	var passwordp = flag.String("password", "", "The password for the email specified.")
	var mailserverp = flag.String("mail-server", "smtp-mail.outlook.com", "The mail server address to authenticate against.")
	var portp = flag.Int("port", 8080, "The port to listen for contact request on.")

	flag.Parse()

	router := httprouter.New()
	router.POST("/contact", func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		log.Println("Contact form submitted on " + r.Host)
		r.ParseForm()
		log.Println(r.Form)

		go sendMail(*emailp, *passwordp, *mailserverp, r)

		w.Write([]byte("Submitted"))
	})
	port := strconv.Itoa(*portp)
	log.Fatal(http.ListenAndServe(":"+port, router))
}

func sendMail(email string, password string, mailserver string, r *http.Request) {

	var site string
	if val, ok := r.Form["site"]; ok {
		site = val[0]
	} else {
		site = "https://www.bethanywhitfield.com"
	}

	var name string
	if val, ok := r.Form["name"]; ok {
		name = val[0]
	}

	var phone string
	if val, ok := r.Form["phone"]; ok {
		phone = val[0]
	}

	var fEmail string
	if val, ok := r.Form["fEmail"]; ok {
		fEmail = val[0]
	}

	var message string
	if val, ok := r.Form["message"]; ok {
		message = val[0]
	}

	auth := smtp.PlainAuth(
		"",
		email,
		password,
		mailserver,
	)

	err := smtp.SendMail(
		mailserver+":587",
		auth,
		email,
		[]string{email},
		[]byte("To: "+email+"\r\n"+
			"Subject: Submission on "+site+"\r\n"+
			"\r\n"+
			"Name: "+name+"\r\n"+
			"Phone: "+phone+"\r\n"+
			"Email: "+fEmail+"\r\n"+
			"Message: "+message+"\r\n"),
	)
	if err != nil {
		log.Fatal(err)
	}
}
